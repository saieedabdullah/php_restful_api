<?php
  class Category{
    private $conn;
    private $table = 'categories';

    // properties
    public $id;
    public $name;
    public $created_at;

    public function __construct($db){
      $this->conn = $db;
    }

    // retreve categories
    public function read(){
      $query = 'SELECT
      id,
      name,
      created_at
      FROM
      ' . $this->table . '
      ORDER BY
      created_at DESC';

      // prepare statement
      $stmt = $this->conn->prepare->($query);

      // execute query
      $stmt->execute();
      return $stmt;
    }
  }

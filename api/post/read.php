<?php
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Post.php';

  // instance DB & connect

  $database = new Database();
  $db = $database->connect();

  // instance post object
  $post = new Post($db);

  // call query function for read
  $result = $post->read();

  // count rows
  $num = $result->rowCount();

  // check any post exist or not
  if ($num > 0) {
    // post array
    $posts_arr = array();
    $posts_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $post_item = array(
        'id' => $id,
        'title' => $title,
        'body' => html_entity_decode($body),
        'author' => $author,
        'category_id' => $category_id,
        'category_name' => $category_name
      );

      // push to "data"
      array_push($posts_arr['data'], $post_item);
    }

    // convert to json & output
    echo json_encode($posts_arr);
  }else {
    echo json_encode(
      array('message' => 'no post found')
    );
  }

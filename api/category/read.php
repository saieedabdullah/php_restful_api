<?php
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Category.php';

  // instance DB & connect

  $database = new Database();
  $db = $database->connect();

  // instance category object
  $category = new Category($db);

  // call query function for read
  $result = $category->read();

  // count rows
  $num = $result->rowCount();

  // check any category exist or not
  if ($num > 0) {
    // cat array
    $cat_arr = array();
    $cat_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $cat_item = array(
        'id' => $id,
        'name' => $name
      );

      // push to "data"
      array_push($cat_arr['data'], $cat_item);
    }

    // convert to json & output
    echo json_encode($cat_arr);
  }else {
    echo json_encode(
      array('message' => 'no category found')
    );
  }

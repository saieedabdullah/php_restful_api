<?php

 class Database{
  private $host = 'localhost';
  private $dbName = 'api_php';
  private $username = 'admin';
  private $password = '1';
  private $conn;

  public function connect(){
    $this->conn = null;

    try {
      $this->conn = new PDO('mysql:host=' . $this->host . ';dbName=' . $this->dbName, $this->username, $this->password);
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
      echo "Connection Error" . $e->getMessage();
    }

    return $this->conn;

  }
}
